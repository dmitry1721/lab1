import os

input_str = os.environ.get("INPUT_STR")

if len(input_str) > 0:
    words_count = len(input_str.split())
    print(f'Input string: {input_str}\nWords Count: {words_count}')
else:
    print('Empty string passed')

