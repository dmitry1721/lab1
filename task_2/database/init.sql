CREATE TABLE IF NOT EXISTS students (
    id SERIAL PRIMARY KEY NOT NULL,
    first_name varchar(255) NOT NULL,
    middle_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    birthday DATE NOT NULL,
    exams varchar(255) NOT NULL
);

INSERT INTO students (first_name, middle_name, last_name, birthday, exams)
VALUES
       ('Петр', 'Александрович', 'Олексин', '2003-08-12', 'Сетевые технологии, Высшая математика'),
       ('Иван', 'Иванович', 'Сидоров', '2001-01-01', 'Математический анализ, История'),
       ('Аркадий', 'Максимович', 'Калашников', '2010-10-05', 'Анализ защищенности, Экономика, Информатика'),
       ('Евгений', 'Петрович', 'Солнцев', '1999-04-11', 'Удаленные сервисы');
