import psycopg2
import os

URL = os.environ.get("POSTGRES_URL")

def execute_query(query):
    try:
        connection = psycopg2.connect(URL)
        cursor = connection.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        connection.commit()

        return result

    except Exception as e:
        print(f"Failed connection: {e}")

def get_youngest_student():
    result = execute_query("""
        SELECT first_name, last_name, birthday
            FROM students
            ORDER BY birthday DESC LIMIT 1;
    """)

    print(f"Task Output: Youngest student - {result}")


def get_oldest_student():
    result = execute_query("""
        SELECT first_name, last_name, birthday
            FROM students
            ORDER BY birthday LIMIT 1;
    """)

    print(f"Task Output: Oldest student - {result}")


if __name__ == "__main__":
    print("Task started")

    get_youngest_student()
    get_oldest_student()
